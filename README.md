## Amplicon primer design

Amplicon primer design is a Python script for the creation of PCR
 primer pairs in a list of target regions.

## Dependencies

Third party binaries

- blastn (NCBI-BLAST+)  

The following python packages are used:

- argparse
- bioPython 1.77+
- numpy 1.18+
- pandas 1.01+
- primer3 0.6+
- subprocess
- sys
- textwrap

## Usage

```bash
usage: ampDesign.py -t TARGET_LIST -o OUTPUT -r REFERENCE [-k KNOWN_SNPS]
                    [-n NO_OF_AMPLI] [-l AMPLI_LENGTH] [-h]

required named arguments:
  -t TARGET_LIST, --target_list TARGET_LIST
                        Bed file with the list of target regions. The file
                        should include at least four columns: Chr, ChrStart,
                        ChrEnd, Name. All the other columns are ignored
                        (default: None)
  -o OUTPUT, --output OUTPUT
                        Output File (default: None)
  -r REFERENCE, --reference REFERENCE
                        FastA file with the reference genome (default: None)

optional arguments:
  -k KNOWN_SNPS, --known_snps KNOWN_SNPS
                        Plink BIM file with the known SNPs position on the
                        reference genome (default: None)
  -n NO_OF_AMPLI, --no-of-ampli NO_OF_AMPLI
                        Number of amplicons for each target (default: 10)
  -l AMPLI_LENGTH, --ampli-length AMPLI_LENGTH
                        Amplicon optimal length (default: 150)
  -h, --help            show this help message and exit

```

## Contributing
Pull requests are welcome. For major changes, please open an issue first 
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)