#!/usr/local/bin/python3.6
# encoding: utf-8
"""
 Amplicon primer design-- Design a set of primer pairs in a list of regions

@author:     Diego Micheletti

@copyright:  2019 FEM. All rights reserved.

@license:    [MIT](https://choosealicense.com/licenses/mit/)

"""

import argparse
import pandas as pd
import numpy as np
from textwrap import dedent
from primer3 import bindings
from Bio import SeqIO
import subprocess as ps
import sys


def get_opt():
    """Command line options."""
    try:
        # Setup argument parser
        parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter,add_help=False)
        required = parser.add_argument_group('required named arguments')
        required.add_argument('-t', '--target_list', type=str, required=True,
                            help=dedent("""\
                                            Bed file with the list of target regions. The file should include at least
                                            four columns: Chr, ChrStart, ChrEnd, Name. All the other columns are ignored
                                            """))
        required.add_argument('-o', '--output', type=str, required=True, help='Output File')
        required.add_argument('-r', '--reference', type=str, required=True,
                            help='FastA file with the reference genome')
        optional = parser.add_argument_group('optional arguments')
        optional.add_argument('-k', '--known_snps', type=str,
                            help='Plink BIM file with the known SNPs position on the reference genome')
        optional.add_argument('-n', '--no-of-ampli', type=int, default=10, help='Number of amplicons for each target')
        optional.add_argument('-l', '--ampli-length', type=int, default=150, help='Amplicon optimal length')
        optional.add_argument('-h', '--help', action='help', default=argparse.SUPPRESS,
                              help='show this help message and exit')

        if len(sys.argv) == 1:
            parser.print_help()
            sys.exit(1)
        else:
            return parser.parse_args()

    except Exception as e:
        print('\nUnexpected error. Read the help\nErrorType:', e)
        # parser.print_help()
        return 2


def prim_par(amplen):
    """
    Primer3 parameters
    :param amplen: amplicon length
    :return: dictionary of parameters to pass to Primer3
    """
    return {
            'PRIMER_OPT_SIZE': 20,
            'PRIMER_PICK_INTERNAL_OLIGO': 0,
            'PRIMER_MIN_SIZE': 18,
            'PRIMER_MAX_SIZE': 25,
            'PRIMER_OPT_TM': 60.0,
            'PRIMER_MIN_TM': 57.0,
            'PRIMER_MAX_TM': 63.0,
            'PRIMER_MIN_GC': 20.0,
            'PRIMER_MAX_GC': 80.0,
            'PRIMER_MAX_POLY_X': 100,
            'PRIMER_INTERNAL_MAX_POLY_X': 100,
            'PRIMER_SALT_MONOVALENT': 50.0,
            'PRIMER_DNA_CONC': 50.0,
            'PRIMER_MAX_NS_ACCEPTED': 0,
            'PRIMER_MAX_SELF_ANY': 12,
            'PRIMER_MAX_SELF_END': 8,
            'PRIMER_PAIR_MAX_COMPL_ANY': 12,
            'PRIMER_PAIR_MAX_COMPL_END': 8,
            'PRIMER_PRODUCT_SIZE_RANGE': [[amplen - 10, amplen + 10]],
            }


def parse_bim(bimfile):
    """
    Read PLINK like bim file
    :param bimfile: name of the file to open
    :return: dictionary with the chromosome ID as key and the list of positons as value
    """
    snps = dict()
    for line in open(bimfile):
        (ch, pos) = line.strip().split()[1].split('_')

        try:
            snps[ch].append(int(pos))
        except KeyError:
            snps[ch] = [int(pos)]
    return dict((k, np.array(v)) for k, v in snps.items())


def seq_with_snp(seq, start, end, snps):
    """
    Replace known ambiguous positions with N
    :param seq:
    :param start:
    :param end:
    :param snps: list of SNPs positions
    :return:
    """
    seq = list(seq)
    for pos in snps:
        if start <= pos <= end:
            seq[pos - start] = 'N'
    return ''.join(seq)


def draw_primer(seq, no_of_ampli, pc, leng, amplen, fout, forout, revout, seqid, primers, rand=True):
    """
    Primer design using the primer3 library
    :param seq: sequence in which prmers should be searched
    :param no_of_ampli: Number of amplicons
    :param pc: amplicon index
    :param leng: amplicon length
    :param amplen:
    :param fout:
    :param forout:
    :param revout:
    :param seqid:
    :param primers:
    :param rand:
    :return:
    """

    if rand:  # random positions
        s = int(rand[pc]) + 1
    else:  # Equidistant frangments
        s = int(pc * len(seq) / no_of_ampli) + 1
    if s + leng > len(seq):
        s = len(seq) - leng
    try:
        prim = bindings.designPrimers({'SEQUENCE_ID': seqid,
                                       'SEQUENCE_TEMPLATE': seq,
                                       'SEQUENCE_INCLUDED_REGION': [s, leng]},
                                      prim_par(amplen))
        try:
            tmp = [prim['PRIMER_LEFT_0_TM'],
                   prim['PRIMER_RIGHT_0_TM'],
                   prim['PRIMER_LEFT_0_GC_PERCENT'],
                   prim['PRIMER_RIGHT_0_GC_PERCENT'],
                   prim['PRIMER_PAIR_0_PRODUCT_SIZE'],
                   prim['PRIMER_LEFT_0_SEQUENCE'],
                   prim['PRIMER_RIGHT_0_SEQUENCE']]
            print(seqid, pc, s,
                  '\t'.join([str(t) for t in tmp]),
                  sep='\t', file=fout
                  )
            primers[seqid + '_' + str(pc)] = [s] + tmp
            print('>{0}_{1!s}\n{2}'.format(seqid, pc, prim['PRIMER_LEFT_0_SEQUENCE']), file=forout)
            print('>{0}_{1!s}\n{2}'.format(seqid, pc, prim['PRIMER_RIGHT_0_SEQUENCE']), file=revout)
        except KeyError:
            if prim['PRIMER_PAIR_NUM_RETURNED'] == 0:
                print(seqid, pc, s, 'No good primer', sep='\t', file=fout)
            else:
                print(seqid, pc, s, 'KeyError', prim)

    except OSError as err:
        print(seqid, pc, s, leng, 'OSError\n', err)
    return primers


def parse_blast(fin):
    """
    Parse tabular  blast output (-outfmt "6 std qlen slen") to keep hsp with identity > 95% and query aln length = to
    query length.
    :param fin: Input file
    :return:
        dictionary with a list of best hit for each query.
    """
    blhits = {}
    for line in open(fin):
        line = line.strip().split()
        if float(line[2]) > 95 and int(line[3]) == int(line[-2]):
            try:
                blhits[line[0]].append(line[1:])
            except KeyError:
                blhits[line[0]] = [line[1:]]
    return blhits


def get_strand(data):
    """
    get the strand from a blast hsp
    :param data: blast hsp as list
    :return: strand '+' if query aln start < end

    """
    if int(data[7]) > int(data[8]):
        return '+'
    else:
        return '-'


def primer_classifier(blf, blr):
    """
    Primer classification based on blast hits of forward and reverse primer.
    :param blf: forward primer best hits
    :param blr: reverse primer best hits
    :return:
        :good: dictionary with the good quality primers
        :bad: set of regions with no good primers
    """
    good, bad = {}, set()
    for k, vf in blf.items():
        try:
            vr = blr[k]
        except KeyError:
            print(k)
            continue
        for fhit in vf:
            for rhit in vr:
                if fhit[0] == rhit[0]:
                    sf, sr = get_strand(fhit), get_strand(rhit)
                    maxpos = max(list(map(int, fhit[7:9] + rhit[7:9])))
                    minpos = min(list(map(int, fhit[7:9] + rhit[7:9])))
                    if sf != sr:
                        if 100 < maxpos - minpos < 300:
                            try:
                                good[k].append([fhit[0], fhit[7:9], rhit[7:9]])
                            except KeyError:
                                good[k] = [[fhit[0], fhit[7:9], rhit[7:9]]]
                        else:
                            bad.add(k)
                    else:
                        bad.add(k)
                else:
                    bad.add(k)
    return good, bad


def main():
    args = get_opt()
    # Get target positions to a dataframe from a bed file file
    targets = pd.read_csv('so58178958.bed', sep='\t', comment='#', header=None)
    header = ['chrom', 'chromStart', 'chromEnd', 'name', 'score', 'strand', 'thickStart', 'thickEnd', 'itemRgb',
              'blockCount', 'blockSizes', 'blockStarts']
    targets.columns = header[:len(targets.columns)]
    # Get index from a fasta file with the gene sequence
    ref_gen = SeqIO.index(args.reference, "fasta")
    # define the amplicon length
    amplen = args.ampli_length
    # Bim (Plink format) file with the list of known SNPs
    if args.known_snps:
        if args.known_snps[-4:] == '.bim':
            snp_pos = parse_bim(args.known_snps)
    else:
        snp_pos = {}
    # dictionary with the primers and relative positions
    primers = {}
    # Create 3 files with forward and reverse primer to blast and one with the information on the primers from primer3
    with open(args.output + '.f.fasta', 'w') as forout, open(args.output + '.r.fasta', 'w') as revout, \
            open(args.output + '.txt', 'w') as fout:
        for i, target in targets.iterrows():
            chrid = target.loc['chrom']
            start = target.loc['chromStart']-1  # to make the position 0 based
            end = target.loc['chromEnd']-1  # to make the position 0 based
            seqid = target.loc['name']
            try:
                snp_chr = snp_pos[chrid]
                seq = seq_with_snp(str(ref_gen[chrid][start:end].seq), start, end, snp_chr)
            except KeyError:
                seq = str(ref_gen[chrid][start:end].seq)
            leng = int(len(seq)/args.no_of_ampli)
            if leng < amplen+50:
                leng = amplen+50
            rand = list(np.random.randint(0, len(seq) - (leng - 1), args.no_of_ampli))
            for pc in range(args.no_of_ampli):
                primers = draw_primer(seq, args.no_of_ampli, pc, leng, amplen,
                                      fout, forout, revout, seqid, primers,
                                      rand=rand)
    # Blast forward primers
    cmd = "blastn -query {0} -db {1} -outfmt '6 std qlen slen' -out {0}.tab.bl -task blastn-short -num_threads 4"\
        .format(args.output + '.f.fasta', args.reference)
    print(cmd.split())
    ps.run(cmd, shell=True)
    # Blast reverse primers
    cmd = "blastn -query {0} -db {1} -outfmt '6 std qlen slen' -out {0}.tab.bl -task blastn-short -num_threads 4" \
        .format(args.output + '.r.fasta', args.reference)
    ps.run(cmd, shell=True)

    blf = parse_blast('{0}.tab.bl'.format(args.output + '.f.fasta'))
    blr = parse_blast('{0}.tab.bl'.format(args.output + '.r.fasta'))
    good, bad = primer_classifier(blf, blr)

    if len(bad.difference(good.keys())) > 0:  # != len(bad):
        print('At least one region does not has a good primer pair', bad.difference(good.keys()))

    with open(args.output + '_goodpairs.txt', 'w') as goodout:
        print('\t'.join(['AmpliconID', 'amplicon start', 'Tmelting F', 'Tmelting R', 'GC F',
                         'GC R', 'amplicon size', 'SeqF', 'SeqR',
                         'blast pos (Chr, (start F, endF),(startR, endR))']), file=goodout)
        for i, (k, v) in enumerate(good.items()):
            print(k, '\t'.join([str(t) for t in primers[k]]), '\t'.join([str(t) for t in v]), file=goodout, sep='\t')


if __name__ == "__main__":
    main()
